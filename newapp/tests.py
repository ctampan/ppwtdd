from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
from unittest import skip

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story6UnitTest(TestCase):

    def landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def profile_url_is_exist(self):
        response = Client().get('profile')
        self.assertEqual(response.status_code,200)

    def form_url_is_exist(self):
        response = Client().get('form')
        self.assertEqual(response.status_code,200)

    def list_url_is_exist(self):
        response = Client().get('list')
        self.assertEqual(response.status_code,200)

    def test_story6_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_landing_page_content_is_written(self):
        request = HttpRequest()
        response = landing(request)
        landing_page_content = response.content.decode('utf8')
        self.assertIsNotNone(landing_page_content)
        self.assertIn("Hello apa kabar?", landing_page_content)

    def check_status(self):
        Message.objects.create(status="tes")
        isi = Message.objects.all().values()
        self.assertContains(isi, "tes")

class VisitorTest (TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable') 
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless') 
        chrome_options.add_argument('disable-gpu') 
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(VisitorTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://ctampan-tdd.herokuapp.com/form')
        time.sleep(5)
        search_box = self.browser.find_element_by_name('status')
        search_box.send_keys('Testing')
        search_box.submit()
        time.sleep(5)
        self.assertIn( "Testing", self.browser.page_source)
