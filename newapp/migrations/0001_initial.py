# Generated by Django 2.1.2 on 2018-10-10 17:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=27)),
                ('created_date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
