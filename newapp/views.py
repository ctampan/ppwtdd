from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import Message

# Create your views here.
landing_text = "Hello apa kabar?"
landing_subtext = "Semoga baik-baik saja."
response = {}
def landing(request) :
    response = {'landing_text' : landing_text, 'landing_subtext' : landing_subtext}
    return render(request, 'landing.html', response)

def profile(request):
    return render(request, 'profile.html')

def form(request):
    response['message_form']=PostForm
    return render(request, 'form.html', response)

def action(request):
    if request.method == "POST":
        response['status']=request.POST['status']
        project = Message(status = response['status'])
        project.save()
        return HttpResponseRedirect('list')
    else:
        return HttpResponseRedirect('list')

def list(request):
    data = Message.objects.all()
    response['data'] = data
    return render(request, 'form_list.html', response)

def clear(request):
    data = Message.objects.all()
    data.delete()
    response['data'] = data
    return HttpResponseRedirect('list')
