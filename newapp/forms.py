from django import forms


class PostForm(forms.Form):
    error_message = {
        'required' : 'Please fill the form',
        'invalid' : 'Incorrect input',
        }

    status = forms.CharField(label='Status', required=False, max_length=27,widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Status'}))
    #empty_Value='Anonymous',
