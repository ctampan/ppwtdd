from django.urls import path
from newapp import views

urlpatterns = [
    path("", views.landing, name="landing"),
    path("profile", views.profile, name="profile"),
    path("form", views.form, name="form"),
    path("process", views.action, name="process"),
    path("list", views.list, name="list"),
    path("clear", views.clear, name="clear")
    ]
